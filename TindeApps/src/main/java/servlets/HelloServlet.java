package servlets;

import dao.InMemoryUserDAO;
import templateFile.TemplateWriteFile;
import userdata.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet (loadOnStartup = 1, urlPatterns = "/*")

public class HelloServlet extends HttpServlet {
    InMemoryUserDAO swithcer = new InMemoryUserDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
       User thisUser = swithcer.ifChosen();

       if (thisUser == null){
           resp.sendRedirect("/liked");
           return;
       }

        TemplateWriteFile.write("hello.html", resp.getWriter(), thisUser);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String posting = req.getParameter("btn");
        User user = swithcer.ifChosen();





        if(posting.equals("Yes")){
           InMemoryUserDAO.likedList.add(user);
        }else if(posting.equals("No")) {
           InMemoryUserDAO.unliked.add(user);
        }
//        swithcer.userAddByID(Integer.valueOf(posting));
        resp.sendRedirect("/*");
    }
}