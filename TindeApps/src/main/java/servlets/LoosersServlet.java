package servlets;

import dao.InMemoryUserDAO;
import userdata.User;
import ua.danit.UserDao;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static dao.InMemoryUserDAO.unliked;

public class LoosersServlet extends HelloServlet {
    UserDao swithcer = new InMemoryUserDAO();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter rider = resp.getWriter();
        rider.write("<html><body>");
        for(User user : unliked) {
            rider.write("<p>"+user.name+"</p>");
        }
        rider.write("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
