package servlets;

import com.google.common.collect.ImmutableMap;
import dao.InMemoryUserDAO;
import templateFile.TemplateWriteFile;
import userdata.User;
import ua.danit.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

@WebServlet(urlPatterns = "/liked")
public class SelectedUsersServlet extends HelloServlet {
    UserDao userDao = new InMemoryUserDAO();
    Set<User> users = InMemoryUserDAO.likedList;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

/*
        Integer IDuser = Integer.getInteger((String)req.getAttribute("userID"));
        String userNameid = (String) req.getAttribute("userName");
        Set<User> users1  = userDao.showByMatch(IDuser);*/

        TemplateWriteFile.write("LikedUsers.html", resp.getWriter(), ImmutableMap.of("users", users, "Man", users.iterator().next()));

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("chatter");
        InMemoryUserDAO.chatbox = name;
        resp.sendRedirect("/chat");
    }
}
