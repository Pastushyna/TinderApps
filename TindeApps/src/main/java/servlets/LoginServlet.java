package servlets;

import templateFile.TemplateWriteFile;
import ua.danit.AuthFilter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@WebServlet(name = "login", loadOnStartup = 1, urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    public static Map<String, String> tokens =new HashMap<>();
    public static Map<String,String> knownUsers = new HashMap<String, String>() {{
        put("Merlin", "123");
        put("Elon", "123");
        put("Dart", "123");
        put("Gomer", "123");
        put("Myron", "123");
        put("Vova", "123");

    }};


@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    TemplateWriteFile.write("login.html", resp.getWriter(), knownUsers);
}


@Override
    protected  void doPost(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {

    String userName=req.getParameter("userName");
    String userID= req.getParameter("userID");
    String password=req.getParameter("password");

    boolean known = knownUsers.containsKey(userName)
            && knownUsers.get(userName).equals(password);



    if(known){

        String token = UUID.randomUUID().toString();
        tokens.put(token, userID);
        req.setAttribute("userID", tokens.get(token));
        resp.addCookie(new Cookie("user-token", token));

        AuthFilter.cookieMap.put(token, userName);

        resp.sendRedirect("/*");
    }else{
        resp.sendRedirect("/login");
    }
}

}


