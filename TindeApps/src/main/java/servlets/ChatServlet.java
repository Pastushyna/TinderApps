package servlets;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import dao.InMemoryUserDAO;
import templateFile.TemplateWriteFile;
import userdata.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet (loadOnStartup = 1, urlPatterns = "/chat")
public class ChatServlet extends HttpServlet{

    InMemoryUserDAO chatterDAO = new InMemoryUserDAO();


    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        User chatter =chatterDAO.forChatting(InMemoryUserDAO.likedList);
      ArrayList<String> npe = Lists.newArrayList("Hello", "How do you do?");
      List<String> arr = chatter.getMessages();

      if(arr.isEmpty()){
          arr.add("");
      }
      TemplateWriteFile.write("chat.html", resp.getWriter(), ImmutableMap.of("user", chatter, "message", arr));


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User chatter =chatterDAO.forChatting(InMemoryUserDAO.likedList);
    String message = req.getParameter("message");

    chatter.messages.add(message);
    resp.sendRedirect("/chat");

    }
}
