package ua.danit;
import servlets.LoginServlet;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@WebFilter(filterName = "cookieFilter", urlPatterns = "/*")
public class AuthFilter implements Filter{

    static Set<String> publicsUrls = new HashSet<String>() {{
        add("/login");
    }};

    public static Map<String, String> cookieMap = new HashMap<>();



    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        Cookie [] cookies = req.getCookies();
        boolean loggedin = false;

       if (cookies != null) {
           for (Cookie cookie : cookies) {
               String name = cookie.getName();
               String token = cookie.getValue();
             //  System.out.printf("Cookie { '%s' : '%s'}\n", name, token);

             //  System.out.println(cookieMap.get(token));


               if (name.equals("user-token") && cookieMap.containsKey(token)) {
                   loggedin = true;
                  req.setAttribute("userName", cookieMap.get(token));

break;

               }

           }
       }

        req.setAttribute("loggedIn", loggedin);

        if(loggedin || publicsUrls.contains(req.getRequestURI())) {
            chain.doFilter(req, resp); //(request, response);
        }else{
            resp.sendRedirect("/login");
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
