package ua.danit;

import userdata.User;

import java.sql.SQLException;
import java.util.Set;

public interface UserDao {


    void userAddByID(Integer userAddByID,Integer id) throws SQLException;

    void setUnlikedD(Integer curUserId, Integer id);

    Set<User> showByMatch(Integer curUserId);

    User ifChosen();

     User forChatting(Set<User> userYes);

}
