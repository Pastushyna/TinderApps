package ua.danit;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.*;

import javax.servlet.DispatcherType;
import java.util.List;


public class App {

    public static void main(String[] args) throws Exception {
        Server server = new Server(7070);
        ServletContextHandler handler = new ServletContextHandler();
        ServletHolder holder = new ServletHolder(new HelloServlet());
        handler.addServlet(holder, "/*");
        
        ServletHolder holderSecond = new ServletHolder(new SelectedUsersServlet());
        handler.addServlet(holderSecond, "/liked");


        ServletHolder holderNo = new ServletHolder(new LoosersServlet());
        handler.addServlet(holderNo, "/no");

        ServletHolder holderLogin = new ServletHolder(new LoginServlet());
        handler.addServlet(holderLogin, "/login");

        ServletHolder holderChat = new ServletHolder(new ChatServlet());
        handler.addServlet(holderChat, "/chat");

        AuthFilter authFilter = new AuthFilter();
        handler.addFilter(new FilterHolder(authFilter), "/*",
                Sets.newEnumSet(Lists.newArrayList(DispatcherType.REQUEST), DispatcherType.class));

        server.setHandler(handler);
        server.start();
        server.join();
    }
}