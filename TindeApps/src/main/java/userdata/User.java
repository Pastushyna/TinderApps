package userdata;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User {


    public int id;
    public String name ;
    public  String imgUrl;
    public List<String> messages;

    public User(int id, String name, String imgUrl) {
        this.id = id;
        this.name = name;
        this.imgUrl = imgUrl;
        this.messages = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(name, user.name) &&
                Objects.equals(imgUrl, user.imgUrl);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, imgUrl, id);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }


    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public List<String> getMessages() {
        return messages;
    }


}
