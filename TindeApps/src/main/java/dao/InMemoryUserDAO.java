package dao;

import db.DBUtil;
import userdata.User;
import ua.danit.UserDao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class InMemoryUserDAO implements UserDao {

   public static HashMap<Integer, User> userList = new HashMap<>();
   public static Set<User> likedList = new HashSet<>();
   public static List<User> unliked = new ArrayList<>();
   public static String chatbox;


    {
        userList.put(1, new User(1, "Elon Mask", "https://cdn.cnn.com/cnnnext/dam/assets/161221145423-cnn-heroes-thumb-elon-musk-full-169.jpg"));
        userList.put(2, new User(2,"Merlin Monroe", "https://24smi.org/public/media/2017/4/20/05_9u7mtxS.jpg") );
        userList.put(3, new User(3,"Dart Vader", "https://lumiere-a.akamaihd.net/v1/images/Darth-Vader_6bda9114.jpeg?region=0%2C23%2C1400%2C785&width=768") );
        userList.put(4, new User(4,"Gomer S", "https://avatanplus.com/files/resources/mid/57dba55b1484e15731fdebcc.png") );
        userList.put(5, new User(5,"VD", "https://pbs.twimg.com/profile_images/622596715036737536/fjxu4qf-.jpg") );
        userList.put(7, new User(7,"Myron Banyas", "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-9/11163785_1112647792085470_8242784467567307631_n.jpg?oh=ff4d680210280b1b9892d826fa11a608&oe=5B0EC8D0") );
    }

//    public void userAddByID(Integer id, Integer curUserId) {
//
//        likedList.add(userList.get(id));
//    }


    @Override
    public void userAddByID(Integer curUserId, Integer id) throws SQLException {



        String addLikebyID = " INSERT INTO `matched` (`likeowner`, `likeed`) VALUES ('"+ curUserId + " ', '"+ id + "')";

        Connection connection = DBUtil.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(addLikebyID);
//        ResultSet resultSet = statement.executeQuery(String.format(useraAddbyID, curUserId));
        ;
    }

    @Override
    public void setUnlikedD(Integer curUserId, Integer id) {

    }

    @Override
    public Set<User> showByMatch(Integer curUserId) {
        return null;
    }

    public void setUnlikedD(Integer id) {
        unliked.add(userList.get(id));

    }


    public Set<User> showByMatch() { return likedList; }

    @Override
    public User ifChosen() {
        for(User user: userList.values()) {
            if(!likedList.contains(user)&&!unliked.contains(user)) return user;
        }
        return null;//likedList.iterator().next();
    }

    @Override
    public  User forChatting(Set<User> userYes) {
        for(User user: userYes) {
            if(user.name.equals(chatbox)) {
                return user;
            }
        }
        return null;
    }


}
